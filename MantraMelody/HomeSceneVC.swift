//
//  ViewController.swift
//  GymGenie
//
//  Created by Apple on 03/05/23.
//

import UIKit
import AVFoundation

class HomeSceneVC: UIViewController {
    
    @IBOutlet weak var cvMainList: UICollectionView!

    let arrMain: [MainModel] = [
        
    //MARK: 1
        
    MainModel(name: "Shloka", img: "1", title: "Shloka", subTitle: """
śloka means "song", from the root śru, "hear" and is a poetic form used in Sanskrit. In its usual form it consists of four pādas or quarter-verses, of 8 syllables each, or two half-verses of 16 syllables each. The meter is similar to the Vedic anuṣṭubh meter, but with stricter rules. The śloka is the basis for Indian epic verse and is used in the Bhagavad Gita, the Mahabharata, the Ramayana, the Puranas and Smritis to name a few.
""", list: [
    ListModel(name: "Achyutashtakam", title: "", img: "1 1", desc: """
    अच्युतं केशवं रामनारायणं कृष्णदामोदरं वासुदेवं हरिम् ।
    श्रीधरं माधवं गोपिकावल्लभं जानकीनायकं रामचन्द्रं भजे ॥ १॥
    acyutaṃ keśavaṃ rāmanārāyaṇaṃ kṛṣṇadāmodaraṃ vāsudevaṃ harim .
    śrīdharaṃ mādhavaṃ gopikā vallabhaṃ jānakī nāyakaṃ rāmacandraṃ bhaje .. 1..
    I sing in praise of Ramachandra, Who is known as Achyuta (infallible), Keshav, Rāma, Narayana, Krishna, Damodara, Vasudeva, Hari, Shridhara (possessing Lakshmi), Madhava, Gopikavallabha (Dearest of Gopika), and Janakinayaka (Lord of Janaki or Sita).
    """),
    ListModel(name: "Acyutam Kesavam", title: "", img: "1 2", desc: """
    अच्युतं केशवं सत्यभामाधवं माधवं श्रीधरं राधिकाराधितम् ।
    इन्दिरामन्दिरं चेतसा सुन्दरं देवकीनन्दनं नन्दनं सन्दधे ॥ २॥
    acyutaṃ keśavaṃ satyabhāmādhavaṃ mādhavaṃ śrīdharaṃ rādhikārādhitam .
    indirā mandiraṃ cetasā sundaraṃ devakī nandanaṃ nandanaṃ sandadhe .. 2..
    I offer a salute with my hands together to Achyuta, who is known as Keshava, who is the consort of Satyabhama (Krishna), who is known as Madhava and Shridhar, who is longed-for by Radhika, who is like a temple of Lakshmi (Indira), who is beautiful at heart, who is the son of Devaki, and who is the dear-one of all.
    """),
    ListModel(name: "Adau Devaki Devi", title: "", img: "1 3", desc: """
    विष्णवे जिष्णवे शङ्खिने चक्रिणे रुक्मिणिऱागिणे जानकीजानये ।
    वल्लवीवल्लभायाऽर्चितायात्मने कंसविध्वंसिने वंशिने ते नमः ॥ ३॥
    viṣṇave jiṣṇave śaṅkhine cakriṇe rukmiṇi ṟāgiṇe jānakījānaye .
    vallavī vallabhāyā’rcitāyātmane kaṃsa vidhvaṃsine vaṃśine te namaḥ .. 3..
    Salutations for Vishnu, who conquers everyone, Who holds a conch-shell and a discus, who is the affectionate of Rukmini (Krishna), who is the consort of only Janaki (Rāma), who is the dear-one of cowherdesses, who is offered [in sacrifices], who is the Atman, who is the destroyer of Kamsa, and who plays the flute (Krishna).
    """),
    ListModel(name: "Adau Rama Tapovanadi", title: "", img: "1 4", desc: """
    कृष्ण गोविन्द हे राम नारायण श्रीपते वासुदेवाजित श्रीनिधे ।
    अच्युतानन्त हे माधवाधोक्षज द्वारकानायक द्रौपदीरक्षक ॥ ४॥
    kṛṣṇa govinda he rāma nārāyaṇa śrīpate vāsudevājita śrīnidhe .
    acyutānanta he mādhavādhokṣaja dvārakānāyaka draupadī rakṣaka .. 4..
    O Krishna! O Govinda! O Rāma! O Narayana! O Shripati! O Vasudeva, who attained the Lakshmi! O Achyuta, who is immeasurable! O Madhava! O Adhokshaja! O Leader of Dvarika! O the protector of Draupadi!
    """),
    ListModel(name: "Aditya Hrudayam", title: "", img: "1 5", desc: """
    राक्षसक्षोभितः सीतया शोभितो दण्डकारण्यभूपुण्यताकारणः ।
    लक्ष्मणेनान्वितो वानरैः सेवितोऽगस्त्यसम्पूजितो राघवः पातु माम् ॥ ५॥
    rākṣasakṣobhitaḥ sītayā śobhito daṇḍakāraṇya bhū puṇyatā kāraṇaḥ .
    lakṣmaṇenānvito vānaraiḥ sevito’gastya sampūjito rāghavaḥ pātu mām .. 5..
    Raghava, who upsetted the demons, who adorned Sita, who is the cause of purification of the forest called Dandaka, who was accompanied by Lakshman, who was served by monkeys, and who was revered by Agastya, save me.
    """),
    ListModel(name: "Agajanana Padmakam", title: "", img: "1 6", desc: """
    धेनुकारिष्टकोऽनिष्टकृद्द्वेषिणां केशिहा कंसहृद्वण्शिकावादकः ।
    पूतनाकोपकः सूरजाखेलनो बालगोपालकः पातु माम् सर्वदा ॥ ६॥
    dhenukāriṣṭako’niṣṭa kṛddveṣiṇāṃ keśihā kaṃsa hṛdvaṇśikāvādakaḥ .
    pūtanākopakaḥ sūrajākhelano bālagopālakaḥ pātu mām sarvadā .. 6..
    Baby Gopala (Krishna), who destroyed the disguised Dhenuka and Arishtak demons, who slayed Keshi, who killed Kansa, who plays the flute, and who got angry on Putana, save me always.
    """),
    ListModel(name: "Agni Suktam", title: "", img: "1 7", desc: """
    विद्युदुद्योतवत्प्रस्फुरद्वाससं प्रावृडम्भोदवत्प्रोल्लसद्विग्रहम् ।
    वन्यया मालया शोभितोरःस्थलं लोहिताङ्घ्रिद्वयं वारिजाक्षं भजे ॥ ७॥
    vidyududyotavatprasphuradvāsasaṃ prāvṛḍambhodavatprollasadvigraham .
    vanyayā mālayā śobhitoraḥsthalaṃ lohitāṅghridvayaṃ vārijākṣaṃ bhaje .. 7..
    I sing praise of the lotus-eyed lord, who is adorned by a shiny lightening like yellow robe, whose body is resplending like a cloud of the rainy-season, who is adorned by a forest-garland at his chest, and who has two feet of copper-red color.
    """),
    ListModel(name: "Ahalya Draupadi Sita Tara", title: "", img: "1 8", desc: """
    कुञ्चितैः कुन्तलैर्भ्राजमानाननं रत्नमौलिं लसत्कुण्डलं गण्डयोः ।
    हारकेयूरकं कङ्कणप्रोज्ज्वलं किङ्किणीमञ्जुलं श्यामलं तं भजे ॥ ८॥
    kuñcitaiḥ kuntalairbhrājamānānanaṃ ratnamauliṃ lasatkuṇḍalaṃ gaṇḍayoḥ .
    hārakeyūrakaṃ kaṅkaṇaprojjvalaṃ kiṅkiṇīmañjulaṃ śyāmalaṃ taṃ bhaje .. 8..
    I sing praise of that Shyam, whose face is adorned by falling locks of curly tresses, who has jewels are forehead, who has shiny earrings on the cheeks, who is adorned with a garland of the Keyur flower, who has a shiny bracelet, and who has a melodious anklet.
    """),
]),
    
    
    //MARK: 2
    
    MainModel(name: "Stotra", img: "2", title: "Stotra", subTitle: """
Means "ode, eulogy or a hymn of praise". A stotra can be a prayer, a description, or a conversation, but always with a poetic structure. It may be a simple poem expressing praise and personal devotion to a deity, or poems with embedded spiritual and philosophical doctrines. Many stotra hymns praise aspects of the divine, such as Devi, Shiva, or Vishnu. They are made up of individual Shlokas.
""", list: [
    ListModel(name: "Annapurna Stotram", title: "", img: "2 1", desc: """
    नित्यानन्दकरी वराभयकरी सौन्दर्यरत्नाकरी
    निर्धूताखिलघोरपावनकरी प्रत्यक्षमाहेश्वरी ।
    प्रालेयाचलवंशपावनकरी काशीपुराधीश्वरी
    भिक्षां देहि कृपावलम्बनकरी माताऽन्नपूर्णेश्वरी ॥ १॥
    nityānandakarī varābhayakarī saundaryaratnākarī
    nirdhūtākhilaghorapāvanakarī pratyakṣamāheśvarī .
    prāleyācalavaṃśapāvanakarī kāśīpurādhīśvarī
    bhikṣāṃ dehi kṛpāvalambanakarī mātā’nnapūrṇeśvarī .. 1..
    Oh! Mother Annapurna! renderer of the support of compassion, the bestower of eternal happiness, the donor of gifts and protection, the ocean of beauty, the destroyer of all sins and purifier, the great goddess, the purifier of the family of Himavan, and the great deity of Kasi, (thou) grant us alms.
    """),
    ListModel(name: "Argala Stotram", title: "", img: "2 1", desc: """
    जय त्वं देवि चामुण्डे जय भूतापहारिणि ।
    जय सर्वगते देवि कालरात्रि नमोऽस्तु ते ॥१॥
    Jaya ṭvam ḍevi Cāmunndde Jaya Bhū-ṭāpa-ḥārinni |
    Jaya Sarva-ġate ḍevi k͟hālarātri ṇamostu ṭe ||1||
    Victory to you, O devi Chamundi, victory to you who are the remover of worldly afflictions and sorrows. Victory to you O devi, who are present in all beings; Salutations to you, O devi Kalaratri (a form of devi Durga, literally means the dark night).
    """),
    ListModel(name: "Ayigiri Nandini", title: "", img: "2 1", desc: """
    अयि गिरिनंदिनि नंदितमेदिनि विश्वविनोदिनि नंदनुते
    गिरिवरविंध्यशिरोधिनिवासिनि विष्णुविलासिनि जिष्णुनुते ।
    भगवति हे शितिकण्ठकुटुंबिनि भूरिकुटुंबिनि भूरिकृते
    जय जय हे महिषासुरमर्दिनि रम्यकपर्दिनि शैलसुते ॥ १॥
    Ayigiri nandini nanditamedini viśvavinodini naṃdanute
    girivaraviṃdhyaśirodhinivāsini viṣṇuvilāsini jiṣṇunute .
    Bhagavati he śitikaṇṭhakuṭuṃbini bhūrikuṭuṃbini bhūrikṛte
    jaya jaya he mahiṣāsuramardini ramyakapardini śailasute .. 1..
    1. Mother Durga – The Daughter of the Mountain and Joy of the World
    O daughter of the mountain, who makes the whole earth happy, who makes the whole universe rejoice, praised by Nandin. Dwelling on the peak of the great Vindhya mountain, glittering widely, (variation? giver of joy to Vishnu), praised by those desirous of victory. O Goddess, wife of the blue necked Siva, One who has many families, One who has done a lot, be victorious, be victorious, O destroyer of the demon mahisa, with beautiful braids of hair, daughter of the mountain Himalaya.
    """),
    ListModel(name: "Dakshinamurthy Stotram", title: "", img: "2 1", desc: """
    ॐ मौनव्याख्या प्रकटितपरब्रह्मतत्वंयुवानं
    वर्शिष्ठान्तेवसदृषिगणैरावृतं ब्रह्मनिष्ठैः |
    आचार्येन्द्रं करकलित चिन्मुद्रमानन्दमूर्तिं
    स्वात्मरामं मुदितवदनं दक्षिणामूर्तिमीडे ‖
    oṃ maunavyākhyā prakaṭita parabrahmatatvaṃ yuvānaṃ
    varśiṣṭhānte vasad ṛṣigaṇair āvṛtaṃ brahmaniṣṭhaiḥ |
    ācāryendraṃ karakalita cinmudram ānandamūrtiṃ
    svātmarāmaṃ muditavadanaṃ dakṣiṇāmūrtimīḍe ‖
    I salute Śrī Dakṣiṇāmūrti, the Young Guru, who teaches the knowledge of Brahman through silence, who is surrounded by disciples, who are themselves ṛṣis and scholars in the Vedas. (I worship Śrī Dakṣiṇāmūrti), who is the teacher of teachers, whose hand is held in the sign of knowledge (cin-mudrā), whose nature is fullness, who reveals in himself, and who is ever silent.
    """),
    ListModel(name: "Devi Aparadha Kshamapana Stotram", title: "", img: "2 1", desc: """
    न मत्रं नो यन्त्रं तदपि च न जाने स्तुतिमहो
    न चाह्वानं ध्यानं तदपि च न जाने स्तुतिकथाः ।
    न जाने मुद्रास्ते तदपि च न जाने विलपनं
    परं जाने मातस्त्वदनुसरणं क्लेशहरणम् ॥१॥
    ṇa ṃatram ṇo ẏantram ṭad-āpi Ca ṇa Jāne Stutim-āho
    ṇa Ca-[ā]ahvānam ḍhyānam ṭad-āpi Ca ṇa Jāne Stuti-k͟hathāh |
    ṇa Jāne ṃudrās-ṭe ṭad-āpi Ca ṇa Jāne Vilapanam
    Param Jāne ṃātas-ṭvad-ānusarannam k͟hleśa-ḥarannam ||1||
    (O Mother) neither your mantra, nor yantra (do I know); and alas, not even I know your stutI (eulogy),
    I do not know how to invoke you through dhyana (meditation); (and alas), not even I know how to simply recite your glories (stuti-katha),
    I do not know your mudras (to contemplate on you); (and alas), not even I know how to simply cry for you,
    However, one thing I know (for certain); by following you (somehow through rememberance however imperfectly) will take away all my afflictions (from my mind).
    """),
    ListModel(name: "Ganapati Stotram", title: "", img: "2 1", desc: """
    अगजानन-पद्माकं गजाननम्-अहर्निशम्।
    अनेकदंतं भक्तानाम् एकदन्तम्-उपास्महे ॥१॥
    agajānana-padmākaṃ gajānanam-aharniśam.
    anekadantaṃ bhaktānām ekadantam-upāsmahe ..1..
    As the rays from the lotus-face of Gauri (Devi Parvati) is always on her beloved son Gajanana (Who has the face of an Elephant),
    Similarly, the grace of Sri Ganesha is always on his devotees; Granting their many prayers; the devotees who with deep devotion worship the Ekadanta ( Who has a single tusk).
    शुक्लांबरधरं विष्णुं शशिवर्णं चतुर्भुजम्।
    प्रसन्नवदनं ध्यायेत् सर्व-विघ्नोपशान्तये ॥२॥
    śuklāmbaradharaṃ viṣṇuṃ śaśivarṇaṃ caturbhujam.
    prasannavadanaṃ dhyāyet sarva-vighnopaśāntaye ..2..
    We meditate on Sri Vishwaksena, who is wearing white clothes, who is all-pervading, who is bright in appearance like the moon and who is having four arms, who is having a compassionate and gracious face; Let us meditate on him to ward of all obstacles.
    """),
    ListModel(name: "Gangā stotram", title: "", img: "2 1", desc: """
    देवि सुरेश्वरि भगवति गङ्गे त्रिभुवनतारिणि तरलतरङ्गे ।
    शङ्करमौलिविहारिणि विमले मम मतिरास्तां तव पदकमले ॥१॥
    devi sureśvari bhagavati gaṅge tribhuvanatāriṇi taralataraṅge .
    śaṅkaramaulivihāriṇi vimale mama matirāstāṃ tava padakamale ..1..
    Oh Devi Bhagavati Ganga, the goddess of the devas, You liberate the three worlds with the (merciful) waves of your liquid form,
    The pure one who resides in the head of Shankara, may my devotion remain firmly established on your lotus feet.
    भागीरथि सुखदायिनि मातस्तव जलमहिमा निगमे ख्यातः ।
    नाहं जाने तव महिमानं पाहि कृपामयि मामज्ञानम् ॥२॥
    bhāgīrathi sukhadāyini mātastava jalamahimā nigame khyātaḥ .
    nāhaṃ jāne tava mahimānaṃ pāhi kṛpāmayi māmajñānam ..2..
    Oh Mother Bhagirathi, you give joy to all, and the glory of your water is praised in the scriptures,
    I do not know your glory fully, but inspite of my Ignorance, please protect me, O compassionate Mother.
    """),
    ListModel(name: "Guru Paduka Stotram", title: "", img: "2 1", desc: """
    अनन्तसंसार समुद्रतार नौकायिताभ्यां गुरुभक्तिदाभ्याम् ।
    वैराग्यसाम्राज्यदपूजनाभ्यां नमो नमः श्रीगुरुपादुकाभ्याम् ॥ 1 ॥
    anantasaṃsāra samudratāra naukāyitābhyāṃ gurubhaktidābhyām ।
    vairāgyasāmrājyadapūjanābhyāṃ namo namaḥ śrīgurupādukābhyām ॥ 1 ॥
    Salutations to the sandals of my Guru,
    Which is a boat, which helps me,cross the endless ocean of life,
    Which endows me, with the sense of devotion to my Guru,
    And by worship of which, I attain the dominion of renunciation.
    कवित्ववाराशिनिशाकराभ्यां दौर्भाग्यदावां बुदमालिकाभ्याम् ।
    दूरिकृतानम्र विपत्ततिभ्यां नमो नमः श्रीगुरुपादुकाभ्याम् ॥ 2 ॥
    kavitvavārāśiniśākarābhyāṃ daurbhāgyadāvāṃ budamālikābhyām ।
    dūrikṛtānamra vipattatibhyāṃ namo namaḥ śrīgurupādukābhyām ॥ 2 ॥
    Salutations to the sandals of my Guru,
    Which is the ocean of knowledge, resembling the full moon,
    Which is the water, which puts out the fire of misfortunes,
    And which removes distresses of those who prostrate before it.
    """),
]),
    
    //MARK: 3
    
    
    MainModel(name: "Mantra", img: "3", title: "Mantra", subTitle: """
Mantra is a sacred utterance, a potent syllable, word or group of words in Sanskrit, that create vibration and resonance. At its simplest, the word ॐ (Aum, Om) is a mantra known as the Pranava Mantra. In more sophisticated forms, mantras are melodic phrases with spiritual interpretations such as a human longing for truth, reality, wisdom, immortality, peace, love, knowledge and action. e.g. the Gayatri Mantra.
""", list: [
    ListModel(name: "Apyayantu Mamangani", title: "", img: "3 1", desc: """
    ॐ आप्यायन्तु ममाङ्गानि वाक्प्राणश्चक्षुः
    श्रोत्रमथो बलमिन्द्रियाणि च सर्वाणि।
    सर्वम् ब्रह्मोपनिषदम् माऽहं ब्रह्म
    निराकुर्यां मा मा ब्रह्म
    निराकरोद निराकरणमस्त्व निराकरणम् मेऽस्तु।
    तदात्मनि निरते य उपनिषत्सु धर्मास्ते
    मयि सन्तु ते मयि सन्तु।
    ॐ शान्तिः शान्तिः शान्तिः॥
    oṃ āpyāyantu mamāṅgāni vākprāṇaścakṣuḥ
    śrotramatho balamindriyāṇi ca sarvāṇi.
    sarvam brahmopaniṣadam mā’haṃ brahma
    nirākuryāṃ mā mā brahma
    nirākaroda nirākaraṇamastva nirākaraṇam me’stu.
    tadātmani nirate ya upaniṣatsu dharmāste
    mayi santu te mayi santu.
    oṃ śāntiḥ śāntiḥ śāntiḥ..
    May my limbs, speech, vital air, eyes, ears, strength, and all the senses be fully developed, all that is revealed by the Upanishads is Brahman. May I never deny Brahman, May Brahman never disown me, Let there be no repudiation (from Brahman), Let there be no infidelity from my side, May all the dharmas extolled by the Upanishads shine in me, Who am intent on knowing the Self. May they shine in me Om! Peace! Peace! Peace!
    """),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
]),
    
    //MARK: 4
    
    
    MainModel(name: "Sūktam", img: "4", title: "Sūktam", subTitle: """
A Sūktam is a vedic hymn in praise of the deity intended. It praises the deity by mentioning its various attributes and paraphernalia. There are numerous Sūktas. The Purusha Sūktam is seen in all Vedas, it is cited as the essence of all Srutis by Veda Vyasa in the Mahabharata. Other popular Sūktams include Purusha Sūktam, Vishnu Sūktam, Sri Sūktam, Medha Sūktam and Narayana Sūktam
""", list: [
    ListModel(name: "Agni Suktam", title: "", img: "4 1", desc: """
    ॐ अग्निमीळे पुरोहितं यज्ञस्य देवमृत्विजम् ।
    होतारं रत्नधातमम् ॥१॥
    ŏm āgnim-īille Purohitam ẏajnyasya ḍevam-ṟtvijam |
    ḥotāram ṟatna-ḍhātamam ||1||
    I glorify Agni, the high priest of sacrifice, the divine, the ministrant, who is the offerer and possessor of greatest wealth.
    """),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
]),
    
    //MARK: 5
    
    
    MainModel(name: "Sūtra", img: "5", title: "Sūtra", subTitle: """
Sūtra means string or thread. Sūtras are a compilation of short aphoristic statements that are concise expressions of a truth or general principle. Brahma Sūtras (or Vedanta Sutra), composed by Badarayana and contains 555 sūtras that summarize the philosophical and spiritual ideas in the Upanishads. Yoga Sūtras compiled by Patanjali, contain 196 sūtras on Yoga including the eight limbs and meditation.
""", list: [
    ListModel(name: "Brahma Sutra", title: "", img: "5 1", desc: """
    श्री ब्रह्मसूत्राणि॥
    ॥ अथ प्रथमोऽध्यायः॥
    ॐ अथातो ब्रह्मजिज्ञासा ॐ ॥ १.१.१॥
    śrī brahmasūtrāṇi..
    .. atha prathamo’dhyāyaḥ..
    oṃ athāto brahmajijñāsā oṃ .. 1.1.1..
    """),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
]),
    
    //MARK: 6
    
    
    MainModel(name: "Bhagavad Gita", img: "6", title: "Bhagavad Gita", subTitle: """
The Bhagavad Gita, or the Song of the Lord, is a dialogue between Krishna, an incarnation of Vishnu, and his friend and disciple, Arjuna. This dialogue takes place in the Bhishma Parva of the Mahabharata. The Bhagavad Gita is composed of 701 Shlokas (verses) arranged in 18 chapters. It is one of the best-known philosophical texts of Hinduism, and is said to contain the essence of Upanishadic thought.
""", list: [
    ListModel(name: "Srimad Bhagavad Gita - Song of God", title: "", img: "6 1", desc: """
    The Bhagavad Gita, or the Song of the Lord, is a dialogue between Krishna, an incarnation of Vishnu, and his friend and disciple, Arjuna. This dialogue takes place in the Bhishma Parva of the Mahabharata. The Bhagavad Gita is composed of 701 Shlokas (verses) arranged in 18 chapters. It is one of the best-known philosophical texts of Hinduism, and is said to contain the essence of Upanishadic thought.The Bhagavad Gita occurs just before the great battle of Mahabharata begins. The army mustered by the five Pandava brothers was to fight the battle against the army of the Pandava’s cousin, Duryodhana, who had robbed them (the Pandavas) of their rightful kingdom and further, refused to participate in any plans for a compromise. After making all possible attempts to peacefully get back their kingdom, or even the right to own a mere five villages in the kingdom, the Pandava brothers decided to fight a war to gain justice.

    Arjuna, the third of the five Pandava princes, was perhaps the greatest and most renowned warrior-hero in the Pandava army. Before the battle began, both Duryodhana and Arjuna went to Krishna to seek his aid. Krishna said that he would not personally lift weapons and fight in the battle, but the cousins could choose to have him, unarmed, on their side, or to have the use of his large army. Arjuna chose to have Krishna with him, and Duryodhana was delighted to add the vast, skilled army of Krishna to his forces. Krishna agreed to drive Arjuna’s chariot and thus to be with him throughout the battle.

    Just before the fighting commenced, Arjuna asked Krishna to place his chariot between the two armies, so that he could take a good look at his enemy. In the enemy ranks, Arjuna saw his cousins, other relatives and his teachers. At this crucial moment, Arjuna’s attachment to his preceptors and family came to the fore, and doubt entered his mind as to the ‘rightness’ of the battle. In his confusion, he no longer knew the course of action that he should take, and he turned to Krishna for guidance. Krishna talked to him, helping him to examine his own motives and desires, and showing him ways to rise above the limitations of his own personality to do what was best for himself and good for society. This dialogue between Krishna and Arjuna, is the Bhagavad Gita.

    The eighteen chapters of the Bhagavad Gita are classified as ‘Yogas’, starting with the ‘yoga’ of Arjuna’s depression and ending with the Yoga of ‘liberation through renunciation’.
    """),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
]),
    
    //MARK: 7
    
    
    MainModel(name: "Upanishads", img: "7", title: "Upanishads", subTitle: """
The Upanishads, commonly referred to as Vedānta. The concepts of Brahman (the ultimate reality) and Ātman (soul, self) are central ideas in all of the Upanishads, and "know that you are the Ātman" is their thematic focus. One hundred and eight Upanishads are enumerated in the Muktikopanishad of which ten are considered the most important Upanishads from the point of view of Vedantic philosophy.
""", list: [
    ListModel(name: "Ishavasya", title: "", img: "7 1", desc: """
    The Isha Upanishad, which is always regarded as first among the Upanishads derives its name from the first word of the first verse of the same Upanishad. The word “Isa” means the Lord of the Universe. the Upanishad begins with the majestic and triumphant declaration that the whole universe is inhabited by God and belongs to Him. As the name suggests, Isa Upanishad is an Upanishad of Isa or Isvara, the Lord of Creation and the source of all.

    Although the Upanishad has only 18 verses, they sum up the essential beliefs and practices of Hinduism. In fact, spiritually and for the purpose of leading a divine centered life, the 18 verses are as important as the 18 chapters of the Bhagavad Gita.
    """),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
]),
    
    //MARK: 8
    
    
    MainModel(name: "Prakarana Granthas", img: "8", title: "Prakarana Granthas", subTitle: """
Sri Adi Shankaracharya and others have composed many sub-texts or topical texts in simple Sanskrit, called Prakarana Granthas, with the objective of broadening the reach of the message of the Vedas and Upanishads. They explain the creed, the processes, the views and ideas of the Vedanta philosophy, using examples, analogies, illustrations; Sri Adi Shankaracharya composed several Prakarana Granthas.
""", list: [
    ListModel(name: "Aparokshānubhuti", title: "", img: "8 1", desc: """
Aparokshanubhuti, or Direct Experience of the Absolute is an introductory work by Adi Shankaracharya that expounds Advaita Vedanta philosophy. Aparoksha refers to the ‘nearest of the near’, one’s Self. Anubhuti means to realize, to experience. So the word means “Self-realization.” Such realization, unlike the knowledge of objects through sense-perception or inference, is an immediate and direct perception of one’s own Self, which is here indicated by the word Aparoksha.

The central theme of the book is the identity between Jivatman (individual Self) and Paramatman (Universal Self)
"""),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
//    ListModel(name: <#T##String#>, title: <#T##String#>, img: <#T##String#>, desc: <#T##String#>),
])
    
    ]
    
    
    let synthesizer = AVSpeechSynthesizer()
    let def = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
//        for i in 1...7{
//            print(i)
//        }
        
        cvMainList.delegate =  self
        cvMainList.dataSource = self
        
        if def.value(forKey: "user") == nil{
            let vc = storyboard?.instantiateViewController(withIdentifier: "GetStartedVC")as! GetStartedVC
            navigationController?.pushViewController(vc, animated: false)
        }

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        synthesizer.stopSpeaking(at: .immediate)
    }

    @objc func SpeakThis(_ sender: UIButton){
        
        if synthesizer.isSpeaking{
            synthesizer.stopSpeaking(at: .immediate)
        }else{
            let text = arrMain[sender.tag].subTitle
            let utterance = AVSpeechUtterance(string: text)
            synthesizer.speak(utterance)
        }
        
        
    }

}

//MARK: collection view

extension HomeSceneVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        arrMain.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = cvMainList.dequeueReusableCell(withReuseIdentifier: "HomeCell", for: indexPath)as! HomeCell
        cell.lblTitle.text = arrMain[indexPath.row].name
        cell.img.image = UIImage(named: "\(arrMain[indexPath.row].img)")
        cell.btnSpeech.tag = indexPath.row
        cell.btnSpeech.addTarget(self, action: #selector(SpeakThis(_:)), for: .touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = CGSize(width: cvMainList.bounds.width/2 - 10, height: cvMainList.bounds.width/2)
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "DescrSceneVC")as! DescrSceneVC
        vc.title = arrMain[indexPath.row].name
        vc.arrData = arrMain[indexPath.row].list
        navigationController?.pushViewController(vc, animated: true)
    }
    
}

//MARK: cell

class HomeCell: UICollectionViewCell{
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var btnSpeech: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
}
