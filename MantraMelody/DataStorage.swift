//
//  DataStorage.swift
//  MantraMelody
//
//  Created by Nick Joliya on 24/08/23.
//
import Foundation

import Foundation

class MenuManager {
    static let shared = MenuManager() // Singleton instance
    
    struct MenuItem {
        let title: String
        let description: String
        let imageURL: String // You can use URL type if the images are online
        
        init(title: String, description: String, imageURL: String) {
            self.title = title
            self.description = description
            self.imageURL = imageURL
        }
    }
    
    let menuItems: [MenuItem] = [
        MenuItem(title: "Learn SansKrit", description: "Description for Item 4", imageURL: "https://example.com/item4.jpg"),
        MenuItem(title: "Trending Sloks", description: "Description for Item 1", imageURL: "https://example.com/item1.jpg"),
        MenuItem(title: "Tattoo ideas", description: "Description for Item 2", imageURL: "https://example.com/item2.jpg"),
        MenuItem(title: "Text To Voice", description: "Description for Item 3", imageURL: "https://example.com/item3.jpg"),
        MenuItem(title: "History of Sanskrit", description: "Description for Item 5", imageURL: "https://example.com/item5.jpg"),
        MenuItem(title: "About Us", description: "Description for Item 6", imageURL: "https://example.com/item6.jpg"),
        MenuItem(title: "Contact Us", description: "Description for Item 7", imageURL: "https://example.com/item7.jpg"),
        MenuItem(title: "Privacy Policy", description: "Description for Item 8", imageURL: "https://example.com/item8.jpg")
       
    ]
    
    let trandingItems: [MenuItem] = [
        MenuItem(title: "1", description: "Description for Item 1", imageURL: "https://example.com/item1.jpg"),
        MenuItem(title: "2", description: "Description for Item 2", imageURL: "https://example.com/item2.jpg"),
        MenuItem(title: "3", description: "Description for Item 3", imageURL: "https://example.com/item3.jpg"),
        MenuItem(title: "4", description: "Description for Item 4", imageURL: "https://example.com/item4.jpg"),
        MenuItem(title: "5", description: "Description for Item 5", imageURL: "https://example.com/item5.jpg"),
        MenuItem(title: "6", description: "Description for Item 6", imageURL: "https://example.com/item6.jpg"),
        MenuItem(title: "7", description: "Description for Item 7", imageURL: "https://example.com/item7.jpg"),
        MenuItem(title: "8", description: "Description for Item 8", imageURL: "https://example.com/item8.jpg"),
        MenuItem(title: "9", description: "Description for Item 9", imageURL: "https://example.com/item9.jpg"),
        MenuItem(title: "10", description: "Description for Item 10", imageURL: "https://example.com/item10.jpg")
    ]
    
    let tattoIdeasItems: [MenuItem] = [
        MenuItem(title: "t1", description: "Description for Item 1", imageURL: "https://example.com/item1.jpg"),
        MenuItem(title: "t2", description: "Description for Item 2", imageURL: "https://example.com/item2.jpg"),
        MenuItem(title: "t3", description: "Description for Item 3", imageURL: "https://example.com/item3.jpg"),
        MenuItem(title: "t4", description: "Description for Item 4", imageURL: "https://example.com/item4.jpg"),
        MenuItem(title: "t5", description: "Description for Item 5", imageURL: "https://example.com/item5.jpg"),
        MenuItem(title: "t6", description: "Description for Item 6", imageURL: "https://example.com/item6.jpg"),
        MenuItem(title: "t7", description: "Description for Item 7", imageURL: "https://example.com/item7.jpg")
    ]
    
    private init() { } // Private constructor to enforce singleton pattern
}
