//
//  ShareScreenVC.swift
//  GymGenie
//
//  Created by Apple on 04/05/23.
//

import UIKit

class ShareScreenVC: UIViewController {
    
    var UData: ListModel? = nil
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var svMain: UIStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if UData != nil{
            img.image = UIImage(named: UData?.img ?? "icon")
            lblTitle.text = UData?.name
            lblDesc.text = UData?.desc
        }
        
    }
    
    @IBAction func btnShare(_ sender: Any) {
        guard let img = svMain.toImage() else{
            return
        }
        shareImage(image: img, viewController: self)
    }
    
    func shareImage(image: UIImage, viewController: UIViewController) {
        let activityController = UIActivityViewController(activityItems: [image], applicationActivities: nil)
        activityController.popoverPresentationController?.sourceView = viewController.view
        viewController.present(activityController, animated: true, completion: nil)
    }
    
}

//MARK: view to image

extension UIView {
    func toImage() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(bounds.size, isOpaque, 0.0)
        defer { UIGraphicsEndImageContext() }
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        layer.render(in: context)
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}
