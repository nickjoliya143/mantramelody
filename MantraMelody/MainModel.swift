//
//  MainModel.swift
//  GymGenie
//
//  Created by Apple on 03/05/23.
//

import Foundation

struct MainModel{
    let name: String
    let img: String
    let title: String
    let subTitle: String
    let list: [ListModel]
    
}

struct ListModel{
    let name: String
    let title: String
    let img: String
    let desc: String
}
