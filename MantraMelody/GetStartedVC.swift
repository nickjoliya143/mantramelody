//
//  GetStartedVC.swift
//  GymGenie
//
//  Created by Apple on 03/05/23.
//

import UIKit
import WebKit

class GetStartedVC: UIViewController {
    
    @IBOutlet weak var viewGif: UIView!
    let def = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // gif url https://cdn.dribbble.com/users/1183973/screenshots/2931793/media/0c1b87aa43d627033b05ef5c24bdc971.gif
//        let cssString = "<style type='text/css'> div { width: 200px; height: 200px; }</style>"
//        gifWebView.loadHTMLString(cssString, baseURL: nil)
        
        let img = UIImage.gifImageWithName("gif")
        let view = UIImageView()
        view.image = img
        view.frame = CGRect(x: 0, y: 0, width: viewGif.bounds.width, height: viewGif.bounds.height)
        viewGif.addSubview(view)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func btnGetStart(_ sender: Any) {
        def.set(true, forKey: "user")
        navigationController?.popToRootViewController(animated: true)
    }
    
    
    

}
