//
//  DescrSceneVC.swift
//  GymGenie
//
//  Created by Apple on 03/05/23.
//

import UIKit
import AVFoundation

class DescrSceneVC: UIViewController {

    var arrData: [ListModel] = []
    
    @IBOutlet weak var tblSubList: UITableView!
    
    let synthesizer = AVSpeechSynthesizer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblSubList.delegate = self
        tblSubList.dataSource = self
//        SpeakThis(text: "hello")
//        SpeakSanskrit(text: "śrī brahmasūtrāṇi")
    }
    
//    func SpeakThis(text: String){
//        let utterance = AVSpeechUtterance(string: text)
//        synthesizer.speak(utterance)
//    }
    
    override func viewWillDisappear(_ animated: Bool) {
        synthesizer.stopSpeaking(at: .immediate)
    }
    
    
    @objc func SpeakThis(_ sender: UIButton){
        
        if synthesizer.isSpeaking{
            synthesizer.stopSpeaking(at: .immediate)
        }else{
            let text = arrData[sender.tag].name
            let utterance = AVSpeechUtterance(string: text)
            synthesizer.speak(utterance)
        }
        
        
    }

//    func SpeakSanskrit(text: String) {
//        let speechUtterance = AVSpeechUtterance(string: text)
//        let synthesizer = AVSpeechSynthesizer()
//        let voiceIdentifier = "com.apple.ttsbundle.eSpeakVoiceData.sanskrit"
//        let voice = AVSpeechSynthesisVoice(identifier: voiceIdentifier)
//        speechUtterance.voice = voice
//
//        synthesizer.speak(speechUtterance)
//    }
}

extension DescrSceneVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblSubList.dequeueReusableCell(withIdentifier: "SubListCell")as! SubListCell
        cell.lblTitle.text = arrData[indexPath.row].name
        cell.lblDesc.text = arrData[indexPath.row].desc
        cell.img.image = UIImage(named: arrData[indexPath.row].img)
        
        cell.btnSpeakTitle.tag = indexPath.row
        cell.btnSpeakTitle.addTarget(self, action: #selector(SpeakThis(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tblSubList.estimatedRowHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ShareScreenVC")as! ShareScreenVC
        vc.UData = arrData[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)
    }
    
}


class SubListCell: UITableViewCell{
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var btnSpeakTitle: UIButton!
    
//    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
//
//           super.init(style: style, reuseIdentifier: reuseIdentifier)
//           // Set the cell's bottom margin to 10 points
//           self.layoutMargins = UIEdgeInsets(top: 0, left: 0, bottom: 600, right: 0)
//       }
//
//       required init?(coder aDecoder: NSCoder) {
//           super.init(coder: aDecoder)
//           // Set the cell's bottom margin to 10 points
//           self.layoutMargins = UIEdgeInsets(top: 0, left: 0, bottom: 300, right: 0)
//       }
    
}
